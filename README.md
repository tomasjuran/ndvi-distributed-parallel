# ndvi-distributed-parallel

Final assignment for "Distributed Systems and Parallel Programming".
Calculate NDVI on a satellite image using distributed computing techniques, as well as GPU-powered parallel computing.