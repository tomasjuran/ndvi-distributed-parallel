import messages.BalancerMessage;
import messages.BalancerMessageType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import generic.ConnectionMetadata;
import generic.SocketWrapper;
import messages.ClientMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.Job;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class MainClient {
	private static Logger logger = LoggerFactory.getLogger("Client");
	private static ConnectionMetadata balancerMetadata;

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.out.println("Run program with:");
			System.out.println("java -jar client.jar <balancer.json>");
			System.out.println("Example:");
			System.out.println("java -jar client.jar balancer/balancer.json");
			System.exit(0);
		}
		configClient(Files.newBufferedReader(Path.of(args[0])));

		boolean success = false;
		while (!success) {
			SocketWrapper balancer = new SocketWrapper(logger, balancerMetadata.getHost(), balancerMetadata.getPort());
			logger.info("Asking " + balancer.getRemoteString() + " for server list.");
			balancer.send(new BalancerMessage(BalancerMessageType.CONNECT, null));
			logger.info("Sent request to " + balancer.getRemoteString() + ". Awaiting response...");

			BalancerMessage msg = (BalancerMessage) balancer.receive();
			balancer.closeSocket();
			List<ConnectionMetadata> serverList = (List<ConnectionMetadata>) msg.getMsg();
			logger.info("Received server list: " + serverList);

			ConnectionMetadata chosen = serverList.get(0);
			SocketWrapper server = new SocketWrapper(logger, chosen.getHost(), chosen.getPort());
			logger.info("Connecting to Server " + server.getRemoteString());
			server.send(new ClientMessage());
			logger.info("Sent request to Server. Awaiting response...");
			Object o = server.receive();
			if (!(o instanceof Job)) {
				// Request failed, try again
				logger.error("Server " + server.getRemoteString() + " could not fulfill request. Trying again...");
				continue;
			}
			logger.info("Server " + server.getRemoteString() + " sent result. Saving to disk...");
			Job result = (Job) o;
			//System.out.println(Arrays.toString(result.result));
			success = true;
		}

	}

	/**
	 * Reads configuration from file, gets Balancer IP and Port and services available
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configClient(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		balancerMetadata = new ConnectionMetadata(
				main.get("balancer").get("host").asText(),
				main.get("balancer").get("port").asInt()
		);
	}
}
