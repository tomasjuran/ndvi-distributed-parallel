package balancer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import generic.ConnectionMetadata;
import generic.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainBalancer {
	private static final Logger logger = LoggerFactory.getLogger("Balancer");
	private static ConnectionMetadata metadata;

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.out.println("Run program with:");
			System.out.println("java -jar balancer.jar <balancer.json>");
			System.out.println("Example:");
			System.out.println("java -jar balancer.jar balancer/balancer.json");
			System.exit(0);
		}
		configBalancer(Files.newBufferedReader(Path.of(args[0])));

		Server server = new Server(logger, metadata.getPort());
		server.setThreadHandlerClass(BalancerHandler.class);
		server.listen();
	}

	/**
	 * Reads configuration from file,
	 * gets Balancer IP and Port
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configBalancer(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		metadata = new ConnectionMetadata(
				main.get("balancer").get("host").asText(),
				main.get("balancer").get("port").asInt()
		);

	}
}
