package balancer;

import generic.ThreadHandler;
import messages.BalancerMessage;
import messages.BalancerMessageType;
import org.slf4j.Logger;
import server.ServerMetadata;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class BalancerHandler extends ThreadHandler {
	private static List<ServerMetadata> serversAvailable = new ArrayList<>();
	private static ReadWriteLock serverStatusLock = new ReentrantReadWriteLock();
		
	public BalancerHandler(Logger logger, Socket socket, Map<String, Object> args) {
		super(logger, socket, args);
	}

	@Override
	public void run() {
		Object o = receive();
		if (!(o instanceof BalancerMessage)) {
			logger.error("Received message from " + getRemoteString() +
				" but it was not understood. Message was \"" + o + "\"");
			closeSocket();
			return;
		}
		BalancerMessage msg = (BalancerMessage) o;
		switch (msg.getMsgType()) {
		case CONNECT:
			handleClientConnection(msg);
			break;
		case STATUS:
			handleServerConnection(msg);
			break;
		default:
			logger.error("Received message from " + getRemoteString() +
					" but it was not understood. Message was \"" + msg + "\"");
		}
		closeSocket();
	}
	
	/**
	 * Take connections from clients and send list of servers
	 */
	public void handleClientConnection(BalancerMessage msg) {
		logger.info("Client " + getRemoteString() + " requested server list.");
		//TODO create servers on demand
		BalancerMessage response;
		serverStatusLock.readLock().lock();
		try {
			response = new BalancerMessage(BalancerMessageType.CONNECT, BalancerHandler.serversAvailable);
		} finally {
			serverStatusLock.readLock().unlock();
		}
		logger.info("Sending server list to client " + getRemoteString());
		send(response);
	}
	
	/**
	 * Handle server status updates
	 * @param msg message with new metadata to update
	 */
	public void handleServerConnection(BalancerMessage msg) {
		ServerMetadata newStatus = (ServerMetadata) msg.getMsg();
		serverStatusLock.writeLock().lock();
		try {
			if (!serversAvailable.contains(newStatus)) {
				// Add new Server
				serversAvailable.add(newStatus);
				logger.info("Server " + newStatus + " subscribed.");
			}
			Collections.sort(serversAvailable);
			logger.debug("Server list: " + serversAvailable);
		} finally {
			serverStatusLock.writeLock().unlock();
		}
		// Keep alive until Server disconnects
		while (connectionIsAlive()) {
			Object o = receive();
			if (!(o instanceof BalancerMessage)) // ignore
				continue;
			// Update status
			newStatus = (ServerMetadata) ((BalancerMessage) o).getMsg();
			serverStatusLock.writeLock().lock();
			try {
				int i = serversAvailable.indexOf(newStatus);
				logger.info("Server " + newStatus + " updated status: "
						+ serversAvailable.get(i).getStatus().name() + " -> "
						+ newStatus.getStatus().name()
				);
				serversAvailable.set(i, newStatus);
				Collections.sort(serversAvailable);
				logger.debug("Server list: " + serversAvailable);
			} finally {
				serverStatusLock.writeLock().unlock();
			}
		}
		// Connection is closed
		logger.info("Server " + newStatus + " unsubscribed. Removing from available Servers list...");
		serverStatusLock.writeLock().lock();
		try {
			serversAvailable.remove(newStatus);
			logger.debug("Server list: " + serversAvailable);
		} finally {
			serverStatusLock.writeLock().unlock();
		}
		// Socket is closed on caller "run()"
	}

}