package cache;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import generic.ConnectionMetadata;
import generic.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.ServerHandler;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

public class MainCache {
	private static final Logger logger = LoggerFactory.getLogger("Cache");
	private static ConnectionMetadata metadata;

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.out.println("Run program with:");
			System.out.println("java -jar cache.jar <cache.json>");
			System.out.println("Example:");
			System.out.println("java -jar cache.jar cache/cache.json");
			System.exit(0);
		}

		CacheHandler.setLogger(logger);
		configCache(Files.newBufferedReader(Path.of(args[0])));
		CacheHandler.setMetadata(metadata);
		Server server = new Server(logger, metadata.getPort());
		server.setThreadHandlerClass(CacheHandler.class);
		server.listen();
	}

	/**
	 * Reads configuration from file,
	 * gets Cache IP and Port
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configCache(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		metadata = new ConnectionMetadata(
				main.get("cache").get("host").asText(),
				main.get("cache").get("port").asInt()
		);
	}
}
