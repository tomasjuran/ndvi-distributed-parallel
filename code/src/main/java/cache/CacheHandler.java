package cache;

import generic.ConnectionMetadata;
import generic.ThreadHandler;
import messages.CacheMessage;
import messages.ClientMessage;
import messages.TaskCompleted;
import org.slf4j.Logger;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class CacheHandler extends ThreadHandler {
	private static Logger logger;
	private static ConnectionMetadata metadata;
	private static Map<Integer, Map<Integer, TaskCompleted>> completedTasks = new HashMap<>();

	public CacheHandler(Logger logger, Socket socket, Map<String, Object> args) {
		super(logger, socket, args);
		if (metadata == null)
			throw new IllegalArgumentException("Initialize Cache with Host and IP first.");
	}

	public static void setLogger(Logger logger) {
		CacheHandler.logger = logger;
	}

	public static void setMetadata(ConnectionMetadata metadata) {
		CacheHandler.metadata = metadata;
	}

	@Override
	public void run() {
		Object o = receive();
		if (!(o instanceof CacheMessage)) {
			logger.error("Received message from " + getRemoteString() +
					" but it was not understood. Message was \"" + o + "\"");
		} else {
			CacheMessage msg = (CacheMessage) o;
			switch (msg.getMsgType()) {
				case UPDATE:
					handleUpdate(msg);
					break;
				case REQUEST:
					handleRequest(msg);
					break;
				default:
					logger.error("Received CacheMessage from " + getRemoteString() +
							" but it was not understood. CacheMessage was \"" + msg + "\"");
			}
		}
		closeSocket();
	}

	private void handleRequest(CacheMessage msg) {
		int jobId = msg.getJobId();
		logger.info("Server " + getRemoteString() + " is requesting Job " + jobId);
		if (!completedTasks.containsKey(jobId)) {
			logger.info("Job " + jobId + " is not in cache. Sending empty Map");
			send(new HashMap<>());
		} else {
			Map<Integer, TaskCompleted> job = completedTasks.get(jobId);
			logger.info("Job " + jobId + " found in cache. Sending " + job.size() + " Tasks");
			send(job);
		}
	}

	private void handleUpdate(CacheMessage msg) {
		int jobId = msg.getJobId();
		TaskCompleted task = msg.getTask();
		logger.info("Received " + task + " from Server " + getRemoteString());
		if (!completedTasks.containsKey(jobId)) {
			logger.info("Adding new entry for job " + jobId);
			completedTasks.put(jobId, new HashMap<>());
		}
		completedTasks.get(jobId).put(task.id, task);
		logger.info("Added " + task + " to cache");
	}
}
