package server;

import generic.ConnectionMetadata;

public class ServerMetadata extends ConnectionMetadata implements Comparable<ServerMetadata> {
	private static final long serialVersionUID = 6566780015331493602L;
	private ServerStatus status;

	public ServerMetadata(String host, int port, ServerStatus status) {
		super(host, port);
		this.status = status;
	}

	public ServerStatus getStatus() {
		return status;
	}

	public int compareTo(ServerMetadata other) {
		if (other == null) return 1;
		return this.status.compareTo(other.status);
	}

	@Override
	public String toString() {
		return super.toString() + " " + this.status.name();
	}
}
