package server;

import java.io.Serializable;

public class Job implements Serializable {
	private static final long serialVersionUID = 4610877200975279216L;
	public final int jobId;
	public final float[] result;

	public Job(int jobId, float[] result) {
		this.jobId = jobId;
		this.result = result;
	}

	public int getJobId() {
		return jobId;
	}

	public float[] getResult() {
		return result;
	}
}
