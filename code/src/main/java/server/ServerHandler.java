package server;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import generic.ConnectionMetadata;
import generic.SocketWrapper;
import generic.ThreadHandler;
import messages.*;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ServerHandler extends ThreadHandler {
	private static Logger logger;
	private static ConnectionMetadata metadata = null;
	private static String[][] dataFilenames = {{"bin/server/nir.txt", "bin/server/red.txt"}};
	private static SocketWrapper balancer = null;
	private static ConnectionMetadata cache = null;
	private static ConnectionFactory queue = null;
	private static String QUEUE_NAME = null;
	private static int split = 1;
	private static int aliveConnections = 0;
	private static ReadWriteLock aliveLock = new ReentrantReadWriteLock();

	private static List<BlockingQueue<TaskCompleted>> completedTasks = new ArrayList<>();
	private static List<Lock> processingLocks = new ArrayList<>();

	public static final int NIR = 0;
	public static final int RED = 1;

	public ServerHandler(Logger logger, Socket socket, Map<String, Object> args) {
		super(ServerHandler.logger, socket, args);
		if (metadata == null)
			throw new IllegalArgumentException("Initialize Server with Host and IP first.");
		if (balancer == null || !balancer.connectionIsAlive())
			throw new IllegalArgumentException("Server requires a connection to Balancer.");
		if (queue == null)
			throw new IllegalArgumentException("Server requires a connection to Queue.");
		if (QUEUE_NAME == null)
			throw new IllegalArgumentException("Server requires Queue name.");
	}

	public static void setLogger(Logger logger) {
		ServerHandler.logger = logger;
	}

	public static void setMetadata(ConnectionMetadata metadata) {
		ServerHandler.metadata = metadata;
	}

	public static void setDataFilenames(String[][] dataFilenames) {
		ServerHandler.dataFilenames = dataFilenames;
	}

	public static void setBalancer(SocketWrapper balancer) {
		ServerHandler.balancer = balancer;
		announce();
	}

	public static void setCache(ConnectionMetadata cache) {
		ServerHandler.cache = cache;
	}

	public static void setQueue(ConnectionMetadata queue) {
		ServerHandler.queue = new ConnectionFactory();
		ServerHandler.queue.setHost(queue.getHost());
		ServerHandler.queue.setPort(queue.getPort());
	}
	
	public static void setQueueName(String queueName) {
		ServerHandler.QUEUE_NAME = queueName;
	}

	public static void setSplit(int split) {
		ServerHandler.split = split;
	}

	public static Integer getAliveConnections() {
		aliveLock.readLock().lock();
		Integer result = null;
		try {
			result = aliveConnections;
		} finally {
			aliveLock.readLock().unlock();
		}
		return result;
	}

	private static void updateAliveConnections(int num) {
		aliveLock.writeLock().lock();
		try {
			aliveConnections += num;
			logger.info("Connections alive: " + aliveConnections);
		} finally {
			aliveLock.writeLock().unlock();
		}
		announce();
	}

	/**
	 * Send status / announce to balancer
	 */
	private static void announce() {
		ServerMetadata status = new ServerMetadata(
				ServerHandler.metadata.getHost(),
				ServerHandler.metadata.getPort(),
				ServerStatus.getStatus(getAliveConnections())
		);
		logger.info(String.format("Sending status update to Balancer (%s)", status.getStatus().name()));
		balancer.send(new BalancerMessage(BalancerMessageType.STATUS, status));

	}

	@Override
	public void run() {
		Object o = receive();
		if (o == null) {
			logger.error("Received message from " + getRemoteString() +
					" but it was not understood. Message was \"" + o + "\"");
			closeSocket();
			return;
		}
		if (o instanceof ClientMessage) {
			handleClientConnection((ClientMessage) o);
		} else if (o instanceof TaskCompleted) {
			handleWorkerConnection((TaskCompleted) o);
		} else {
			logger.error("Received message from " + getRemoteString() +
					" but it was not understood. Message was \"" + o + "\"");
		}
		closeSocket();
	}

	private void handleClientConnection(ClientMessage msg) {
		updateAliveConnections(1);
		int jobId = 0;
		// Create structures for new job
		if (processingLocks.size() < jobId+1) {
			processingLocks.add(new ReentrantLock());
			completedTasks.add(new LinkedBlockingDeque<TaskCompleted>(split));
		}
		float[][] resultFragments = null;
		//long startTime = System.nanoTime();
		// Load data from disk
		int[][] data = loadData(dataFilenames[jobId]);
		// Split into fragments to distribute
		logger.info("Received request for " + jobId + " from client " + getRemoteString() +
				". Splitting into " + split + " tasks");
		int[][][] fragments = split(data, split);
		// Start processing
		processingLocks.get(jobId).lock();
		try {
			// Check cache for completed tasks
			fragments = checkCache(fragments, jobId);
			// Send tasks to queue
			if (sendToQueue(fragments, jobId)) {
				// Wait for completion
				resultFragments = waitForResult(jobId, split);
			}
		} finally {
			processingLocks.get(jobId).unlock();
		}
		logger.info("All tasks finished for job " + jobId);
		//System.out.println((System.nanoTime() - startTime) / 1000000);
		if (resultFragments != null) {
			// Join result fragments
			float[] result = join(resultFragments);
			// Send response to client
			logger.info("Sending finished job " + jobId + " to client " + getRemoteString());
			send(new Job(jobId, result));
		}
		updateAliveConnections(-1);
	}

	private void handleWorkerConnection(TaskCompleted task) {
		logger.info("Received completed task " + task);
		try {
			completedTasks.get(task.jobId).put(task);
			logger.info("Trying to send completed task to cache " + task);
			SocketWrapper con = new SocketWrapper(logger, cache.getHost(), cache.getPort());
			if (con != null && con.connectionIsAlive()) {
				if (con.send(new CacheMessage(task.jobId, CacheMessageType.UPDATE, task))) {
					logger.info("Sent task " + task + " to cache");
					return;
				}
			}
			logger.error("Sending task " + task + " to cache failed");
			if (con != null && con.connectionIsAlive())
				con.closeSocket();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load data from disk
	 * @param files filenames for NIR and RED data
	 * @return NIR and RED
	 */
	private int[][] loadData(String[] files) {
		int[][] result = new int[2][];
		try {
			result[NIR] = Files.lines(Paths.get(files[NIR]))
					.flatMap(line -> Arrays.stream(line.split(" ")))
					.mapToInt(Integer::valueOf)
					.toArray();
			result[RED] = Files.lines(Paths.get(files[RED]))
					.flatMap(line -> Arrays.stream(line.split(" ")))
					.mapToInt(Integer::valueOf)
					.toArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	private int[][][] split(int[][] data, int n) {
		int[][][] fragments = new int[n][][];
		int size = data[0].length / n;
		int start = 0;
		int end = size;
		int i = 0;
		while (i < n-1) {
			fragments[i] = new int[2][];
			fragments[i][NIR] = Arrays.copyOfRange(data[NIR], start, end);
			fragments[i][RED] = Arrays.copyOfRange(data[RED], start, end);
			start = end;
			end += size;
			i++;
		}
		fragments[i] = new int[2][];
		fragments[i][NIR] = Arrays.copyOfRange(data[NIR], start, data[NIR].length);
		fragments[i][RED] = Arrays.copyOfRange(data[RED], start, data[RED].length);
		return fragments;
	}

	private float[] join(float[][] fragments) {
		List<Float> temp = new ArrayList<>();
		for (float[] fragment : fragments)
			for (float value : fragment)
				temp.add(value);
		float[] result = new float[temp.size()];
		for (int i = 0; i < temp.size(); i++) result[i] = temp.get(i);
		return result;
	}

	/**
	 * Check cache for already processed fragments
	 * @param fragments original fragments
	 * @return fragments to process (minus those found in cache
	 */
	private int[][][] checkCache(int[][][] fragments, int jobId) {
		SocketWrapper con = new SocketWrapper(logger, cache.getHost(), cache.getPort());
		if (con == null || !con.connectionIsAlive()) {
			logger.info("Cache not available");
			return fragments;
		}
		logger.info("Requesting cache for Job " + jobId);
		con.send(new CacheMessage(jobId, CacheMessageType.REQUEST));
		Map<Integer, TaskCompleted> completed = (Map<Integer, TaskCompleted>) con.receive();
		logger.info("Obtained " + completed.size() + " fragments from cache");
		BlockingQueue<TaskCompleted> blockingQueue = ServerHandler.completedTasks.get(jobId);
		for (int taskId : completed.keySet()) {
			TaskCompleted task = completed.get(taskId);
			try {
				logger.info("Saving task " + task);
				blockingQueue.put(task);
				fragments[taskId] = null;
			} catch (InterruptedException e) {
				logger.error("Could not save task " + task);
				e.printStackTrace();
			}
		}
		if (con != null && con.connectionIsAlive())
			con.closeSocket();
		return fragments;
	}

	/**
	 * Create tasks and distribute
	 * @param fragments data fragments
	 * @return True if there are no errors, false otherwise
	 */
	private boolean sendToQueue(int[][][] fragments, int jobId) {
		List<Task> tasks = new ArrayList<>();
		for (int i = 0; i < fragments.length; i++) {
			if (fragments[i] == null) continue;
			Task task = new Task(
					logger,
					metadata,
					jobId,
					i,
					fragments[i][NIR],
					fragments[i][RED]
			);
			tasks.add(task);
		}

		try (Connection connection = ServerHandler.queue.newConnection(); Channel channel = connection.createChannel()) {
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			for (Task task : tasks) {
				channel.basicPublish("", QUEUE_NAME, null, task.toBytes());
				logger.info("Queued task " + task);
			}
		} catch (IOException | TimeoutException e) {
			logger.error("Could not queue new Task while handling client " + getRemoteString());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Wait until all workers are finished
	 * @param jobId jobId to recover
	 * @param split number of fragments to expect
	 * @return ordered result fragments
	 */
	private float[][] waitForResult(int jobId, int split) {
		int received = 0;
		float[][] result = new float[split][];
		BlockingQueue<TaskCompleted> blockingQueue = ServerHandler.completedTasks.get(jobId);
		while (received < split) {
			try {
				TaskCompleted completed = blockingQueue.take();
				result[completed.id] = completed.result;
				received++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
