package server;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import generic.ConnectionMetadata;
import generic.Server;
import generic.SocketWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static server.ServerHandler.NIR;
import static server.ServerHandler.RED;

public class MainServer {
	private static final Logger logger = LoggerFactory.getLogger("Server");
	private static ConnectionMetadata metadata;
	private static int split;
	private static String[][] dataFilenames;
	private static ConnectionMetadata balancer;
	private static ConnectionMetadata cache;
	private static ConnectionMetadata queue;
	private static String queueName;

	public static void main(String[] args) throws IOException {
		if (args.length < 4) {
			System.out.println("Run program with:");
			System.out.println("java -jar server.jar <server.json> <balancer.json> <cache.json> <queue.json>");
			System.out.println("Example:");
			System.out.println("java -jar server.jar server/server.json balancer/balancer.json cache/cache.json queue/queue.json");
			System.exit(0);
		}

		ServerHandler.setLogger(logger);
		configServer(Files.newBufferedReader(Path.of(args[0])));
		ServerHandler.setMetadata(metadata);
		ServerHandler.setDataFilenames(dataFilenames);
		ServerHandler.setSplit(split);

		configBalancer(Files.newBufferedReader(Path.of(args[1])));
		SocketWrapper balancerSocket = new SocketWrapper(logger, balancer.getHost(), balancer.getPort());
		if (balancerSocket == null || !balancerSocket.connectionIsAlive())
			throw new IllegalArgumentException("Server needs a connection to Balancer. " +
					"Check that balancer is accepting requests, and check configuration files.");
		ServerHandler.setBalancer(balancerSocket);

		configCache(Files.newBufferedReader(Path.of(args[2])));
		ServerHandler.setCache(cache);

		configQueue(Files.newBufferedReader(Path.of(args[3])));
		ServerHandler.setQueue(queue);
		ServerHandler.setQueueName(queueName);

		Server server = new Server(logger, metadata.getPort());
		server.setThreadHandlerClass(ServerHandler.class);
		server.listen();
	}

	/**
	 * Reads configuration from file,
	 * gets Server IP and Port,
	 * gets split value (number of tasks a job should be split into)
	 * gets data file locations
	 * @param configFile
	 * @throws IOException
	 */
	public static void configServer(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);
		metadata = new ConnectionMetadata(
				main.get("server").get("host").asText(),
				main.get("server").get("port").asInt()
		);
		split = main.get("split").asInt();
		JsonNode filenames = main.get("dataFilenames");
		dataFilenames = new String[filenames.size()][];
		for (int i = 0; i < filenames.size(); i++) {
			dataFilenames[i] = new String[2];
			dataFilenames[i][NIR] = filenames.get(i).get(NIR).asText();
			dataFilenames[i][RED] = filenames.get(i).get(RED).asText();
		}
	}

	/**
	 * Reads configuration from file,
	 * gets Balancer IP and Port
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configBalancer(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		balancer = new ConnectionMetadata(
				main.get("balancer").get("host").asText(),
				main.get("balancer").get("port").asInt()
		);
	}

	/**
	 * Reads configuration from file,
	 * gets Cache IP and Port
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configCache(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		cache = new ConnectionMetadata(
				main.get("cache").get("host").asText(),
				main.get("cache").get("port").asInt()
		);
	}

	/**
	 * Reads configuration from file,
	 * gets Queue IP and Port,
	 * gets Queue name
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configQueue(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		queue = new ConnectionMetadata(
				main.get("queue").get("host").asText(),
				main.get("queue").get("port").asInt()
		);
		queueName = main.get("queueName").asText();
	}
}