package server;

public enum ServerStatus {
	/**
	 * Has no jobs running
	 */
	NORMAL,
	/**
	 * Should not take new jobs
	 */
	ALERT,
	/**
	 * Cannot take new jobs
	 */
	CRITICAL;

	public static ServerStatus getStatus(int aliveConnections) {
		switch (aliveConnections) {
			case 0:
				return NORMAL;
			case 1:
				return ALERT;
			default:
				return CRITICAL;
		}
	}

	public boolean isOnAlert() {
		switch (this) {
			case NORMAL:
				return false;
			case ALERT:
			case CRITICAL:
			default:
				return true;
		}
	}
}
