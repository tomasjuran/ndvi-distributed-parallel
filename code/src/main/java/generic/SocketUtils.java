package generic;

import java.net.Socket;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 28, 2018
 */
public class SocketUtils {

	public static String getRemoteHostPortString(Socket socket) {
		return socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
	}

	public static String getLocalHostPortString(Socket socket) {
		return socket.getLocalAddress().getHostAddress() + ":" + socket.getLocalPort();
	}
}