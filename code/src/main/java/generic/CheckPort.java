package generic;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Utility to check for port availability
 * @author Martín Tomás Juran
 * @version 1.0.0 Apr 10, 2020
 */
public class CheckPort {
	/**
	 * Returns an available port
	 * @param start try with port {@code start}. If not available, try with {@code start + 1} and so on
	 */
	public static int GetAvailable(int start) {
		while (!IsAvailable(start))
			start++;
		return start;
	}
	
	/**
	 * Returns true if port is available
	 * @param port port to check
	 */
	public static boolean IsAvailable(int port) {
		ServerSocket server = null;
		try {
			server = new ServerSocket(port);
		} catch (IOException ignored) {
			return false;
		}
		try {
			server.close();
		} catch (NullPointerException | IOException ignored) {}
		
		return true;
	}
}
