package generic;

import org.slf4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;

/**
 * @author Martín Tomás Juran
 * @version 2.0.0 Mar 28, 2018
 */
public class Server {	
	protected Logger logger;
	
	protected final int port;
	protected ServerSocket server;
	/**
	 * Class that implements Runnable and takes a Socket as parameter on its Constructor
	 */
	private Class<? extends ThreadHandler> threadHandlerClass;
	/**
	 * Arguments to pass to threadHandler Constructor
	 */
	private Map<String, Object> threadHandlerArgs = null;
	
	public int getPort() {
		return port;
	}
	public ServerSocket getServer() {
		return server;
	}
	public void setThreadHandlerClass(Class<? extends ThreadHandler> threadHandlerClass) {
		this.threadHandlerClass = threadHandlerClass;
	}
	public void setThreadHandlerArgs(Map<String, Object> threadHandlerArgs) {
		this.threadHandlerArgs = threadHandlerArgs;
	}

	/**
	 * @param port port the server listens to
	 * @throws IOException 
	 */
	public Server(Logger logger, int port) throws IOException {
		this.logger = logger;
		this.port = port;
		try {
			this.server = new ServerSocket(port);
		} catch (IOException e) {
			logger.error("Server could not start on port " + port);
			throw new IOException(e);
		}
	}
	
	
	public void listen() {
		logger.info("Server listening on port " + port);
		while (true) {
			accept();
		}
	}
	
	public void listen(int tries) {
		logger.info("Server listening on port " + port + " (max " + tries + " tries)");
		int i = 0;
		while (i < tries) {
			accept();
			i++;
		}
	}
	
	private void accept() {
		try {
			Socket client = server.accept();
			logger.info("Accepted connection from " + SocketUtils.getRemoteHostPortString(client));
			ThreadHandler runnable = (ThreadHandler) threadHandlerClass
					.getConstructor(Logger.class, Socket.class, Map.class)
					.newInstance(this.logger, client, threadHandlerArgs);
			Thread thread = new Thread(runnable);
			thread.start();
			logger.debug("Launched thread for " + SocketUtils.getRemoteHostPortString(client));
		} catch (Exception e) {
			logger.error("Could not establish connection with client");
			e.printStackTrace();
		}
	}
}