package generic;

import org.slf4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author Martín Tomás Juran
 * @version 1.0.0 Mar 28, 2018
 */
public class SocketWrapper {
	protected Logger logger;

	protected Socket socket;
	protected ObjectOutputStream output = null;
	protected ObjectInputStream input = null;

	public SocketWrapper(Logger logger, String remoteAddr, int remotePort) {
		this.logger = logger;
		try {
			socket = new Socket(remoteAddr, remotePort);
			logger.info(SocketUtils.getLocalHostPortString(socket) + " connected to " +
					SocketUtils.getRemoteHostPortString(socket));
			getStreams();
		} catch(IOException e) {
			logger.debug(e.toString());
			logger.error("Could not connect to " + remoteAddr + ":" + remotePort);
		}
	}

	public SocketWrapper(Logger logger, Socket socket) {
		this.logger = logger;
		this.socket = socket;
		getStreams();
	}

	private void getStreams() {
		try {
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			closeSocket();
		}
	}

	public void closeSocket() {
		if (socket != null) {
			try {
				logger.info("Closing connection to " + SocketUtils.getRemoteHostPortString(socket));
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean send(Object obj) {
		if (connectionIsAlive()) {
			try {
				output.writeObject(obj);
				logger.debug("Sent " + obj + " to " + SocketUtils.getRemoteHostPortString(socket));
				return true;
			} catch (Exception e) {
				logger.debug(e.toString());
				logger.error("Send message failed: Closing connection to " + SocketUtils.getRemoteHostPortString(socket));
				closeSocket();
			}
		} else {
			logger.error("Send message failed: Connection is not active");
		}
		return false;
	}

	public Object receive() {
		if (connectionIsAlive()) {
			try {
				Object o = input.readObject();
				logger.debug("Received " + o + " from " + SocketUtils.getRemoteHostPortString(socket));
				return o;
			} catch (Exception e) {
				logger.debug(e.toString());
				logger.error("Receive message failed: Closing connection to " + SocketUtils.getRemoteHostPortString(socket));
				closeSocket();
			}
		} else {
			logger.error("Receive message failed: Connection is not active");
		}
		return null;
	}

	public boolean connectionIsAlive() {
		return socket != null && !socket.isClosed();
	}

	public String getRemoteString() {
		return SocketUtils.getRemoteHostPortString(this.socket);
	}

	public String getLocalString() {
		return SocketUtils.getLocalHostPortString(this.socket);
	}
}

