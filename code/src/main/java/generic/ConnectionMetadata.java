package generic;

import java.io.Serializable;
import java.net.Socket;
import java.util.Objects;

public class ConnectionMetadata implements Serializable {
	private static final long serialVersionUID = 196108753238670016L;
	private final String host;
	private final int port;

	public String getHost() {
		return host;
	}
	public int getPort() {
		return port;
	}

	public ConnectionMetadata(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public ConnectionMetadata(Socket socket) {
		this.host = socket.getInetAddress().getHostAddress();
		this.port = socket.getPort();
	}

	@Override
	public int hashCode() {
		return Objects.hash(host, port);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ConnectionMetadata))
			return false;
		ConnectionMetadata other = (ConnectionMetadata) obj;
		return Objects.equals(host, other.host) && port == other.port;
	}

	@Override
	public String toString() {
		return this.host + ":" + this.port;
	}
}