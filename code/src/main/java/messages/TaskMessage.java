package messages;

import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class TaskMessage implements Serializable {
	private static final long serialVersionUID = -268628876716155026L;
	protected final Logger logger;

	public TaskMessage(Logger logger) {
		this.logger = logger;
	}

	public byte[] toBytes() {
		byte[] result = new byte[]{};
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
			 ObjectOutputStream out = new ObjectOutputStream(bos)) {
			out.writeObject(this);
			out.flush();
			result = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(String.format("Could not serialize %s - %s", this.getClass().getName(), this));
		}
		return result;
	}
}
