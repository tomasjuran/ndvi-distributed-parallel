package messages;

public enum BalancerMessageType {
	/**
	 * Client asks for connection
	 */
	CONNECT,
	/**
	 * Server sends status/subscribes
	 */
	STATUS
}