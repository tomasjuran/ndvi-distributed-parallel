package messages;

import java.io.Serializable;

public class CacheMessage implements Serializable {
	private static final long serialVersionUID = -1984994191935469473L;

	private final int jobId;
	private final CacheMessageType msgType;
	private TaskCompleted task;

	public CacheMessage(int jobId, CacheMessageType msgType) {
		this.jobId = jobId;
		this.msgType = msgType;
	}

	public CacheMessage(int jobId, CacheMessageType msgType, TaskCompleted task) {
		this.jobId = jobId;
		this.msgType = msgType;
		this.task = task;
	}

	public int getJobId() {
		return jobId;
	}

	public CacheMessageType getMsgType() {
		return msgType;
	}

	public TaskCompleted getTask() {
		return task;
	}
}
