package messages;

import org.slf4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class TaskCompleted extends TaskMessage {
	private static final long serialVersionUID = 253576306214709705L;
	public final int jobId;
	public final int id;
	public final int workerId;
	public final float[] result;

	public TaskCompleted(Logger logger, int jobId, int id, int workerId, float[] result) {
		super(logger);
		this.jobId = jobId;
		this.id = id;
		this.workerId = workerId;
		this.result = result;
	}

	public TaskCompleted(Logger logger, byte[] data) throws Exception {
		super(logger);
		try (ByteArrayInputStream bis = new ByteArrayInputStream(data);
			 ObjectInput in = new ObjectInputStream(bis)) {
			TaskCompleted other = (TaskCompleted) in.readObject();
			this.jobId = other.jobId;
			this.id = other.id;
			this.workerId = other.workerId;
			this.result = other.result;
		} catch (IOException | ClassNotFoundException e) {
			logger.error(String.format("Could not decode %s - %s", this.getClass().getName(), new String(data, StandardCharsets.UTF_8)));
			throw e;
		}
	}

	@Override
	public String toString() {
		return "Job: " + this.jobId + " - Task: " + this.id + " - Worker: " + this.workerId;
	}
}
