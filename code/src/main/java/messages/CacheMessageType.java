package messages;

public enum CacheMessageType {
	/**
	 * Server wants to save a completed task
	 */
	UPDATE,
	/**
	 * Server wants to ask for all completed tasks of a job
	 */
	REQUEST
}
