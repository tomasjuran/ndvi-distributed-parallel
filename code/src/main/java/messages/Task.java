package messages;

import generic.ConnectionMetadata;
import org.slf4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;

public class Task extends TaskMessage {
	private static final long serialVersionUID = 3063691245075758626L;
	public final ConnectionMetadata server;
	public final int jobId;
	public final int id;
	public final int[] nir;
	public final int[] red;

	public Task(Logger logger, ConnectionMetadata server, int jobId, int id, int[] nir, int[] red) {
		super(logger);
		this.server = server;
		this.jobId = jobId;
		this.id = id;
		this.nir = nir;
		this.red = red;
	}

	public Task(Logger logger, byte[] data) throws Exception {
		super(logger);
		try (ByteArrayInputStream bis = new ByteArrayInputStream(data);
			 ObjectInput in = new ObjectInputStream(bis)) {
			Task other = (Task) in.readObject();
			this.server = other.server;
			this.jobId = other.jobId;
			this.id = other.id;
			this.nir = other.nir;
			this.red = other.red;
		} catch (IOException | ClassNotFoundException e) {
			logger.error(String.format("Could not decode %s - %s", this.getClass().getName(), new String(data, StandardCharsets.UTF_8)));
			throw e;
		}
	}

	@Override
	public String toString() {
		return "Job:" + this.jobId + " - Task:" + this.id;
	}

}
