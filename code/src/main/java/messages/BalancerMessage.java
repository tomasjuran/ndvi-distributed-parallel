package messages;

import java.io.Serializable;

public class BalancerMessage implements Serializable {
	private static final long serialVersionUID = -5939058291577640224L;
	private final BalancerMessageType msgType;
	private final Object msg;
	
	public BalancerMessage(BalancerMessageType msgType, Object msg) {
		this.msgType = msgType;
		this.msg = msg;
	}

	public BalancerMessageType getMsgType() {
		return msgType;
	}

	public Object getMsg() {
		return msg;
	}
	
	@Override
	public String toString() {
		String s = this.msgType.toString();
		s += (msg == null) ? "" : ": " + this.msg.toString();
		return  s;
	}
}
