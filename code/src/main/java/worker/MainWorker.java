package worker;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import generic.ConnectionMetadata;
import generic.SocketWrapper;
import messages.Task;
import messages.TaskCompleted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class MainWorker {
	private static final Logger logger = LoggerFactory.getLogger("Worker");
	private static int workerId = 0;

	private static ConnectionMetadata queue;
	private static String queueName;

	private static final String BASE_DIR = ".";
	private static final String RUN_SCRIPT = "main";
	private static final String NIR_DATA = "nir.txt";
	private static final String RED_DATA = "red.txt";
	private static final String NDVI_DATA = "ndvi.txt";

	public static void main(String[] args) throws Exception {
		if (args.length > 1) {
			workerId = Integer.parseInt(args[1]);
		}
		configQueue(Files.newBufferedReader(Path.of(args[0])));
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(queue.getHost());
		factory.setPort(queue.getPort());
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.basicQos(1);
		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			try {
				Task task = new Task(logger, delivery.getBody());
				logger.info("Taken task " + task + " from server " + task.server);
				//long elapsed = 0;
				//long startTime = System.nanoTime();
				try (FileWriter writer = new FileWriter(String.valueOf(Paths.get(BASE_DIR, NIR_DATA)))) {
					for (int nir : task.nir) {
						writer.write(nir + " ");
					}
				} catch (IOException e) {
					logger.error(NIR_DATA + " could not be written (check file permissions).");
					throw e;
				}
				try (FileWriter writer = new FileWriter(String.valueOf(Paths.get(BASE_DIR, RED_DATA)))) {
					for (int red : task.red) {
						writer.write(red + " ");
					}
				} catch (IOException e) {
					logger.error(RED_DATA + " could not be written (check file permissions).");
					throw e;
				}

				logger.info("Processing task " + task);
				ProcessBuilder pb = new ProcessBuilder("bash", "-c",
						String.format("./%s %s %s %d %s", RUN_SCRIPT, NIR_DATA, RED_DATA, task.nir.length, NDVI_DATA));
				pb.directory(new File(BASE_DIR));
				//pb.inheritIO();
				Process process = pb.start();
				int exitVal = process.waitFor();
				if (exitVal != 0) {
					logger.error("Could not process task " + task + ". Aborting");
					throw new Exception("Parallel process failed");
				}

				logger.info("Processed task " + task + ". Preparing to send to server " + task.server);
				// Read results
				double[] partial;
				try {
					partial = Files.lines(Paths.get(BASE_DIR, NDVI_DATA))
							.flatMap(line -> Arrays.stream(line.split(" ")))
							.mapToDouble(Double::valueOf)
							.toArray();
				} catch (IOException e) {
					logger.error(NDVI_DATA + " could not be read (check file permissions).");
					throw e;
				}
				float[] result = new float[partial.length];
				for (int i = 0; i < partial.length; i++) {
					result[i] = (float) partial[i];
				}
				//elapsed += System.nanoTime() - startTime;
				//System.out.println(elapsed / 1000000);
				// Send to server
				SocketWrapper server = new SocketWrapper(logger, task.server.getHost(), task.server.getPort());
				if (server.send(new TaskCompleted(logger, task.jobId, task.id, workerId, result)))
					logger.info("Completed task " + task + " sent to server " + task.server + ". Delivering ACK to queue");
				else
					logger.error("Completed task " + task + " but could not be sent to server " +	task.server + ". Freeing queue");
				channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("An error occurred while handling a task. Requeuing...");
				channel.basicReject(delivery.getEnvelope().getDeliveryTag(), true);
			}
		};
		channel.basicConsume(queueName, false, deliverCallback, consumerTag -> { });
	}

	/**
	 * Reads configuration from file,
	 * gets Queue IP and Port,
	 * gets Queue name
	 * @param configFile JSON file with configuration
	 * @throws IOException if file could not be read
	 */
	public static void configQueue(Reader configFile) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode main = mapper.readTree(configFile);

		queue = new ConnectionMetadata(
				main.get("queue").get("host").asText(),
				main.get("queue").get("port").asInt()
		);
		queueName = main.get("queueName").asText();
	}
}
