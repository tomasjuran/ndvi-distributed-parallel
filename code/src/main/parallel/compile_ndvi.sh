#!/usr/bin/env bash

OPENCL="/opt/rocm/opencl"
BIN="../../../bin"

gcc main.c -o $BIN/main -lOpenCL -I$OPENCL/include -L$OPENCL/lib
