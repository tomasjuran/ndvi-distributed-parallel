#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 
#define CL_TARGET_OPENCL_VERSION 200
 
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
 
#define MAX_SOURCE_SIZE (0x100000)

const double CLOCKS_PER_MS = CLOCKS_PER_SEC / 1000;

int main(int argc, char *argv[]) {
  if (argc < 5) {
    printf("Too few arguments. Use this program as:\n\tmain <nir file> <red file> <# of pixels> <result file name>");
    return 1;
  }
  int i;
  
  // Measure time
  srandom(time(NULL));
	clock_t t_start, t_read, t_htod, t_op, t_dtoh, t_write;
	t_start = clock();
  
  // Amount of pixels to read
  const int DATA_SIZE = atoi(argv[3]);
  // Read NIR data
  float *NIR = (float *) malloc(sizeof(float) * DATA_SIZE);
  FILE *fnir = fopen(argv[1], "r");
  if (!fnir) {
    fprintf(stderr, "Failed to load NIR data from %s.\n", argv[1]);
    return 1;
  }
  for (i = 0; i < DATA_SIZE; i++) {
    fscanf(fnir, "%f", &NIR[i]);
  }
  fclose(fnir);
  
  // Read RED data
  float *RED = (float *) malloc(sizeof(float) * DATA_SIZE);
  FILE *fred = fopen(argv[2], "r");
  if (!fred) {
    fprintf(stderr, "Failed to load RED data from %s.\n", argv[1]);
    return 1;
  }
  for (i = 0; i < DATA_SIZE; i++) {
    fscanf(fred, "%f", &RED[i]);
  }
  fclose(fred);

  // Load the kernel source code into the array source_str
  FILE *fk;
  char *source_str;
  size_t source_size;

  fk = fopen("ndvi_kernel.cl", "r");
  if (!fk) {
    fprintf(stderr, "Failed to load kernel.\n");
    return 1;
  }
  source_str = (char *) malloc(MAX_SOURCE_SIZE);
  source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fk);
  fclose(fk);

  /**
   * Time to read data
   **/
  t_read = clock();
  
  
  // Get platform and device information
  cl_platform_id platform_id = NULL;
  cl_device_id device_id = NULL;   
  cl_uint ret_num_devices;
  cl_uint ret_num_platforms;
  cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
  ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, &ret_num_devices);
  // Create an OpenCL context
  cl_context context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
  // Create a command queue
  cl_command_queue command_queue = clCreateCommandQueueWithProperties(context, device_id, 0, &ret);

  // Create memory buffers on the device for each array 
  cl_mem nir_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, 
      DATA_SIZE * sizeof(float), NULL, &ret);
  cl_mem red_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY,
      DATA_SIZE * sizeof(float), NULL, &ret);
  cl_mem ndvi_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 
      DATA_SIZE * sizeof(float), NULL, &ret);

  // Copy NIR and RED values to their respective memory buffers
  ret = clEnqueueWriteBuffer(command_queue, nir_mem_obj, CL_TRUE, 0,
      DATA_SIZE * sizeof(float), NIR, 0, NULL, NULL);
  ret = clEnqueueWriteBuffer(command_queue, red_mem_obj, CL_TRUE, 0, 
      DATA_SIZE * sizeof(float), RED, 0, NULL, NULL);

  // Create a program from the kernel source
  cl_program program = clCreateProgramWithSource(context, 1, 
      (const char**) &source_str, (const size_t*) &source_size, &ret);
  // Build the program
  ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
  // Create the OpenCL kernel
  cl_kernel kernel = clCreateKernel(program, "ndvi_kernel", &ret);
  // Set the arguments of the kernel
  ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*) &nir_mem_obj);
  ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*) &red_mem_obj);
  ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*) &ndvi_mem_obj);

  /**
   * Time to load program and buffers from host to device data
   **/
  t_htod = clock();


  // Execute the OpenCL kernel on the data
  size_t global_item_size = DATA_SIZE;
  //size_t local_item_size = 64; // Divide work items into groups of 64
  ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, 
          &global_item_size, NULL, 0, NULL, NULL);
  //printf("%d\n", ret);

  /**
   * Time to execute program
   **/
  t_op = clock();


  // Read the memory buffer NIR on the device to the local variable NIR
  float *NDVI = (float*) malloc(sizeof(float) * DATA_SIZE);
  ret = clEnqueueReadBuffer(command_queue, ndvi_mem_obj, CL_TRUE, 0, 
      sizeof(float) * DATA_SIZE, NDVI, 0, NULL, NULL);

  /**
   * Time load result from device
   **/
  t_dtoh = clock();


  // Save result to file
  FILE *fndvi;
  fndvi = fopen(argv[4], "w+");
  if (!fndvi) {
    fprintf(stderr, "Could not write result to %s.\n", argv[4]);
    return 1;
  }
  for (i = 0; i < DATA_SIZE; i++)
    fprintf(fndvi, "%.5f ", NDVI[i]);
  fclose(fndvi);
  
  /**
   * Time to save data to disk
   **/
  t_write = clock();
  
  
  // Clean up
  ret = clFlush(command_queue);
  ret = clFinish(command_queue);
  ret = clReleaseKernel(kernel);
  ret = clReleaseProgram(program);
  ret = clReleaseMemObject(nir_mem_obj);
  ret = clReleaseMemObject(red_mem_obj);
  ret = clReleaseMemObject(ndvi_mem_obj);
  ret = clReleaseCommandQueue(command_queue);
  ret = clReleaseContext(context);
  free(NIR);
  free(RED);
  free(NDVI);
  
  //printf("Calculated NDVI for %d pixels. Results saved in %s.", DATA_SIZE, argv[4]);
  printf("%.3f %.3f %.3f %.3f %.3f",
      (double) (t_read - t_start) / CLOCKS_PER_MS,
      (double) (t_htod - t_read) / CLOCKS_PER_MS,
      (double) (t_op - t_htod) / CLOCKS_PER_MS,
      (double) (t_dtoh - t_op) / CLOCKS_PER_MS,
      (double) (t_write - t_dtoh) / CLOCKS_PER_MS
  );
  return 0;
}
