#!/usr/bin/env bash

RUNS=50
DATA_DIR="../../../preprocessing"

for ((i=0; i<RUNS; i++))
do
  ./main $DATA_DIR/nir.txt $DATA_DIR/red.txt 10119624 ndvi.txt
  echo ""
done
