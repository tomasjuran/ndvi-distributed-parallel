__kernel void ndvi_kernel(__global const float *NIR, __global const float *RED, __global float *NDVI) {
 
    // Get the index of the current element to be processed
    int i = get_global_id(0);
 
    // Do the operation
    NDVI[i] = (NIR[i] - RED[i]) / (NIR[i] + RED[i]);
}
