#!/usr/bin/env python3

import rioxarray as rxr

RED = 0
NIR = 3
output = {RED: "red", NIR: "nir"}

naip_data = rxr.open_rasterio("naip.tif")

for band in [RED, NIR]:
  with open(f"{output[band]}.txt", "w") as f:
    for y in naip_data.data[band]:
      for x in y:
        f.write(f"{x} ")
